package net.pl3x.pl3xcommands;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Player;

public class MailBox {
	private static final HashMap<UUID, MailBox> boxes = new HashMap<UUID, MailBox>();

	public static MailBox getMailBox(MyPlugin plugin, UUID uuid) {
		synchronized (boxes) {
			if (boxes.containsKey(uuid)) {
				return boxes.get(uuid);
			}
			MailBox box = new MailBox(plugin, uuid);
			boxes.put(uuid, box);
			return box;
		}
	}

	public static HashMap<UUID, MailBox> getAllMailBoxes() {
		return boxes;
	}

	public static void removeConfig(UUID uuid) {
		synchronized (boxes) {
			if (!boxes.containsKey(uuid)) {
				return;
			}
			boxes.remove(uuid);
		}
	}

	public static void removeAllConfigs() {
		synchronized (boxes) {
			boxes.clear();
		}
	}

	private MyPlugin plugin;
	private Set<Letter> mail = new HashSet<Letter>();
	private UUID uuid;

	public MailBox(MyPlugin plugin, UUID uuid) {
		this.plugin = plugin;
		this.uuid = uuid;
		refreshMail();
	}

	public Set<Letter> refreshMail() {
		mail.clear();
		String raw = PlayerConfig.getConfig(plugin, uuid).get("mail");
		if (raw == null) {
			return mail;
		}
		for (String tool : raw.split(";;")) {
			String[] split = tool.split("::");
			if (split.length < 2) {
				continue;
			}
			String from = split[0];
			String date = "0", message;
			// Temporary backwards compatibility.
			if (split.length > 2) {
				date = split[1];
				message = split[2];
			} else {
				message = split[1];
			}
			mail.add(new Letter(from, date, message));
		}
		return mail;
	}

	public Set<Letter> getMail() {
		refreshMail();
		return mail;
	}

	public void clearMail() {
		PlayerConfig.getConfig(plugin, uuid).set("mail", "");
	}

	public void sendMail(String sender, String message) {
		if (message == null || message.equals("") || sender == null || sender.equals("")) {
			return;
		}
		PlayerConfig config = PlayerConfig.getConfig(plugin, uuid);
		String raw = config.get("mail");
		if (raw != null && !raw.equals("")) {
			raw += ";;";
		}
		raw += sender + "::" + System.currentTimeMillis() + "::" + message;
		config.set("mail", raw);
		refreshMail();
		MC_Player player = Pl3xLibs.getPlayer(uuid);
		if (player == null) {
			return;
		}
		Pl3xLibs.sendMessage(player, Lang.MAIL_NOTIFICATION_ALERT.get());
	}

	public class Letter {
		private String sender;
		private String date; // for future use
		private String message;

		public Letter(String sender, String date, String message) {
			this.sender = sender;
			this.date = date;
			this.message = message;
		}

		public String getSender() {
			return sender;
		}

		public String getDate() {
			return date;
		}

		public String getMessage() {
			return message;
		}

		public String format() {
			return Lang.MAIL_FORMAT.get().replace("{player}", sender).replace("{date}", date.equals(0) ? "unknown" : Pl3xLibs.epochToTimeStamp(Long.valueOf(date))).replace("{message}", message);
		}
	}
}
