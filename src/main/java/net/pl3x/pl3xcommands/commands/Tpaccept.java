package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Tpaccept implements MC_Command {
	public Tpaccept(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.TPACCEPT.get();
	}

	@Override
	public String getCommandName() {
		return "tpaccept";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7tpaccept &a- &d" + Lang.TPACCEPT_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (Tpa.tpadb.containsKey(player.getUUID())) {
			MC_Player requester = Pl3xLibs.getPlayer(Tpa.tpadb.get(player.getUUID()));
			if (requester == null) {
				// This should never happen because the request would have been removed when the requester went offline
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			}
			requester.teleport(player.getLocation());
			Pl3xLibs.sendMessage(requester, Lang.TPACCEPT_REQUESTER_MSG.get().replace("{player}", player.getCustomName()));
			Pl3xLibs.sendMessage(player, Lang.TPACCEPT_PLAYER_MSG.get());
			Tpa.tpadb.remove(player.getUUID());
			return;
		}
		if (Tpahere.tpaheredb.containsKey(player.getUUID())) {
			MC_Player requester = Pl3xLibs.getPlayer(Tpahere.tpaheredb.get(player.getUUID()));
			if (requester == null) {
				// This should never happen because the request would have been removed when the requester went offline
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			}
			player.teleport(requester.getLocation());
			Pl3xLibs.sendMessage(requester, Lang.TPACCEPT_REQUESTER_MSG.get());
			Pl3xLibs.sendMessage(player, Lang.TPACCEPT_PLAYER_MSG.get());
			Tpahere.tpaheredb.remove(player.getUUID());
			return;
		}
		Pl3xLibs.sendMessage(player, Lang.TPACCEPT_NO_PENDING.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.tpaccept");
	}
}
