package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Nick implements MC_Command {
	private MyPlugin plugin;

	public Nick(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.NICK.get();
	}

	@Override
	public String getCommandName() {
		return "nick";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.colorize("/nick <nickname> (player) - " + Lang.NICK_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.nick.other")) {
			return Pl3xLibs.colorize("&e/&7nick &e<&7nickname&e> (&7player&e) &a- &d" + Lang.NICK_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7nick &e<&7nickname&e> &a- &d" + Lang.NICK_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 2) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[1]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String nick = args[0].trim();
		if (nick == null || nick.equals("")) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		if (args.length > 1) {
			if (player != null && !player.hasPermission("command.nick.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			MC_Player target = Pl3xLibs.getPlayer(args[1]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.nick.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.NICK_TARGET_EXEMPT.get());
				return;
			}
			if (nick.equalsIgnoreCase("none") || nick.equalsIgnoreCase("clear") || nick.equalsIgnoreCase("off") || nick.equalsIgnoreCase("remove") || nick.equalsIgnoreCase("delete")) {
				nick = "";
			}
			if (!target.hasPermission("command.nick.diffname") && !Pl3xLibs.decolorize(target.getName()).equals(Pl3xLibs.decolorize(nick))) {
				Pl3xLibs.sendMessage(player, Lang.NICK_TARGET_NO_NAMECHANGE.get().replace("{target}", target.getCustomName()));
				return;
			}
			if (!target.hasPermission("command.nick.colors") && Pl3xLibs.hasColor(nick)) {
				Pl3xLibs.sendMessage(player, Lang.NICK_TARGET_NO_COLORS.get().replace("{target}", target.getCustomName()));
				return;
			}
			if (!target.hasPermission("command.nick.styles") && Pl3xLibs.hasStyle(nick)) {
				Pl3xLibs.sendMessage(player, Lang.NICK_TARGET_NO_STYLES.get().replace("{target}", target.getCustomName()));
				return;
			}
			target.setCustomName(nick);
			PlayerConfig.getConfig(plugin, target.getUUID()).set("nickname", nick);
			Pl3xLibs.sendMessage(player, Lang.NICK_SET_OTHER.get().replace("{target}", target.getCustomName()));
			return;
		}
		if (player == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_NO_CONSOLE.get());
			return;
		}
		if (nick.equalsIgnoreCase("none") || nick.equalsIgnoreCase("clear") || nick.equalsIgnoreCase("off") || nick.equalsIgnoreCase("remove") || nick.equalsIgnoreCase("delete")) {
			nick = "";
		}
		if (!player.hasPermission("command.nick.diffname") && !Pl3xLibs.decolorize(player.getName()).equals(Pl3xLibs.decolorize(nick))) {
			Pl3xLibs.sendMessage(player, Lang.NICK_NO_NAMECHANGE.get());
			return;
		}
		if (!player.hasPermission("command.nick.colors") && Pl3xLibs.hasColor(nick)) {
			Pl3xLibs.sendMessage(player, Lang.NICK_NO_COLORS.get());
			return;
		}
		if (!player.hasPermission("command.nick.styles") && Pl3xLibs.hasStyle(nick)) {
			Pl3xLibs.sendMessage(player, Lang.NICK_NO_STYLES.get());
			return;
		}
		player.setCustomName(nick);
		PlayerConfig.getConfig(plugin, player.getUUID()).set("nickname", nick);
		Pl3xLibs.sendMessage(player, Lang.NICK_SET.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.nick");
	}
}
