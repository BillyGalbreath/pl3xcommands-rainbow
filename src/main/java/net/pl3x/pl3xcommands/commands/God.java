package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class God implements MC_Command {
	private MyPlugin plugin;

	public God(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.GOD.get();
	}

	@Override
	public String getCommandName() {
		return "god";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/god (player) - " + Lang.GOD_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.god.other")) {
			return Pl3xLibs.colorize("&e/&7god &e(&7player&e) &a- &d" + Lang.GOD_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7god &a- &d" + Lang.GOD_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length > 0) {
			if (player != null && !player.hasPermission("command.god.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			MC_Player target = Pl3xLibs.getPlayer(args[0]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.god.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.GOD_TARGET_EXEMPT.get());
				return;
			}
			boolean enabled = PlayerConfig.getConfig(plugin, target.getUUID()).toggleGodMode(); // toggle it
			target.setInvulnerable(enabled);
			Pl3xLibs.sendMessage(target, Lang.GOD_TOGGLED.get().replace("{toggle}", enabled ? "enabled" : "disabled"));
			Pl3xLibs.sendMessage(player, Lang.GOD_TOGGLED_OTHER.get().replace("{target}", target.getCustomName()).replace("{toggle}", enabled ? "enabled" : "disabled"));
			return;
		}
		boolean enabled = PlayerConfig.getConfig(plugin, player.getUUID()).toggleGodMode(); // toggle it
		player.setInvulnerable(enabled);
		Pl3xLibs.sendMessage(player, Lang.GOD_TOGGLED.get().replace("{toggle}", enabled ? "enabled" : "disabled"));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.god");
	}
}
