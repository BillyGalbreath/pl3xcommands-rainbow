package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Tptoggle implements MC_Command {
	private MyPlugin plugin;

	public Tptoggle(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.TPTOGGLE.get();
	}

	@Override
	public String getCommandName() {
		return "tptoggle";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7tptoggle &a- &d" + Lang.TPTOGGLE_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		PlayerConfig config = PlayerConfig.getConfig(plugin, player.getUUID());
		boolean tpDisabled = !config.getBoolean("tp-disabled");
		config.set("tp-disabled", tpDisabled ? "true" : "false");
		if (tpDisabled) {
			Pl3xLibs.sendMessage(player, Lang.TPTOGGLE_DISABLED.get());
		} else {
			Pl3xLibs.sendMessage(player, Lang.TPTOGGLE_ENABLED.get());
		}
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.tptoggle");
	}
}
