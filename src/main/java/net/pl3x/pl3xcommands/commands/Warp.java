package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Location;
import PluginReference.MC_Player;

public class Warp implements MC_Command {
	private MyPlugin plugin;

	public Warp(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.WARP.get();
	}

	@Override
	public String getCommandName() {
		return "warp";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/warp <warp> (player) - " + Lang.WARP_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.warp.other")) {
			return Pl3xLibs.colorize("&e/&7warp &e<&7warp&e> (&7player&e) &a- &d" + Lang.WARP_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7warp &e<&7warp&e> &a- &d" + Lang.WARP_HELP_DESC_SELF.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return plugin.getWarpConfig().getMatchingWarpsList(args[0]);
		}
		if (args.length == 2) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[1]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, plugin.getWarpConfig().listWarps());
			return;
		}
		String warpName = args[0].trim().toLowerCase();
		MC_Location warp = plugin.getWarpConfig().getWarp(warpName);
		if (warp == null) {
			Pl3xLibs.sendMessage(player, Lang.WARP_NO_WARP_FOUND.get());
			return;
		}
		if (plugin.getConfig().getBoolean("use-warp-permissions") && !player.hasPermission("command.warp." + warpName)) {
			Pl3xLibs.sendMessage(player, Lang.WARP_NO_PERM.get());
			return;
		}
		MC_Player target = player;
		if (args.length > 1) {
			if (!player.hasPermission("command.warp.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			target = Pl3xLibs.getPlayer(args[1]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.warp.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.WARP_TARGET_EXEMPT.get());
				return;
			}
			Pl3xLibs.sendMessage(player, Lang.WARP_OTHER_MSG.get().replace("{target}", target.getCustomName()).replace("{warp}", warpName));
		}
		target.teleport(warp);
		Pl3xLibs.sendMessage(player, Lang.WARP_SELF_MSG.get().replace("{warp}", warpName));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.warp");
	}
}
