package net.pl3x.pl3xcommands.commands;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Tpa implements MC_Command {
	private MyPlugin plugin;
	public static HashMap<UUID, UUID> tpadb = new HashMap<UUID, UUID>();

	public Tpa(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.TPA.get();
	}

	@Override
	public String getCommandName() {
		return "tpa";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7tpa &e<&7player&e> &a- &d" + Lang.TPA_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.TP_NO_PLAYER.get());
			return;
		}
		MC_Player target = Pl3xLibs.getPlayer(args[0]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		if (PlayerConfig.getConfig(plugin, target.getUUID()).getBoolean("tp-disabled") && !player.hasPermission("command.tp.override")) {
			Pl3xLibs.sendMessage(player, Lang.TP_PLAYER_DISABLED_TP.get());
		}
		tpadb.put(target.getUUID(), player.getUUID());
		Pl3xLibs.sendMessage(target, Lang.TPA_TARGET_MSG1.get().replace("{player}", player.getCustomName()));
		Pl3xLibs.sendMessage(target, Lang.TPA_TARGET_MSG2.get());
		Pl3xLibs.sendMessage(player, Lang.TPA_PLAYER_MSG.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.tpa");
	}
}
