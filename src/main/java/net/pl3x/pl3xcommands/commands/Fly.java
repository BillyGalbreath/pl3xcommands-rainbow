package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Fly implements MC_Command {
	public Fly(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.FLY.get();
	}

	@Override
	public String getCommandName() {
		return "fly";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/fly (player) - " + Lang.FLY_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.fly.other")) {
			return Pl3xLibs.colorize("&e/&7fly &e(&7player&e) &a- &d" + Lang.FLY_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7fly &a- &d" + Lang.FLY_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length > 0) {
			if (player != null && !player.hasPermission("command.fly.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			MC_Player target = Pl3xLibs.getPlayer(args[0]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.fly.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.FLY_TARGET_EXEMPT.get());
				return;
			}
			boolean enable = !target.isAllowedFlight();
			target.setAllowFlight(enable);
			if (!enable) {
				// if disabling, stop player from flying
				target.setFlying(false);
			}
			Pl3xLibs.sendMessage(target, Lang.FLY_MODE_TOGGLED.get().replace("{toggle}", enable ? "enabled" : "disabled"));
			Pl3xLibs.sendMessage(player, Lang.FLY_MODE_TOGGLED_OTHER.get().replace("{target}", target.getCustomName()).replace("{toggle}", enable ? "enabled" : "disabled"));
			return;
		}
		boolean enable = !player.isAllowedFlight();
		player.setAllowFlight(enable);
		if (!enable) {
			// if disabling, stop player from flying
			player.setFlying(false);
		}
		Pl3xLibs.sendMessage(player, Lang.FLY_MODE_TOGGLED.get().replace("{toggle}", enable ? "enabled" : "disabled"));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.fly");
	}
}
