package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Tpaall implements MC_Command {
	private MyPlugin plugin;

	public Tpaall(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.TPAALL.get();
	}

	@Override
	public String getCommandName() {
		return "tpaall";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7tpaall &a- &d" + Lang.TPAALL_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		for (MC_Player target : plugin.getServer().getPlayers()) {
			if (target.equals(player)) {
				continue; // do not teleport self
			}
			if (PlayerConfig.getConfig(plugin, target.getUUID()).getBoolean("tp-disabled") && !player.hasPermission("command.tp.override")) {
				continue; // target has tp disabled
			}
			Tpahere.tpaheredb.put(target.getUUID(), player.getUUID());
			Pl3xLibs.sendMessage(target, Lang.TPA_TARGET_MSG1.get());
			Pl3xLibs.sendMessage(target, Lang.TPA_TARGET_MSG2.get());
		}
		Pl3xLibs.sendMessage(player, Lang.TPAALL_PLAYER_MSG.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.tpaall");
	}
}
