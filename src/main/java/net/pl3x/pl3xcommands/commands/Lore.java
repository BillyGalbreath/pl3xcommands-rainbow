package net.pl3x.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Player;

public class Lore implements MC_Command {
	public Lore(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.LORE.get();
	}

	@Override
	public String getCommandName() {
		return "lore";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7lore &e<&7lore to set&e> &a- &d" + Lang.LORE_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.LORE_NO_LORE.get());
			return;
		}
		MC_ItemStack hand = player.getItemInHand();
		if (hand == null || hand.getId() == 0) {
			Pl3xLibs.sendMessage(player, Lang.LORE_NO_ITEM.get());
			return;
		}
		String loreString = Pl3xLibs.colorize(Pl3xLibs.join(args, " ", 0));
		List<String> lore = new ArrayList<String>();
		if (loreString.contains("\\n")) {
			for (String line : loreString.split("\\\\n")) {
				lore.add(Pl3xLibs.colorize(line));
			}
		} else {
			if (!loreString.equalsIgnoreCase("none") && !loreString.equalsIgnoreCase("clear") && !loreString.equalsIgnoreCase("remove") && !loreString.equalsIgnoreCase("delete")) {
				lore.add(Pl3xLibs.colorize(loreString));
			}
		}
		hand.setLore(lore);
		player.setItemInHand(hand);
		if (lore.isEmpty()) {
			Pl3xLibs.sendMessage(player, Lang.LORE_CLEARED.get());
		} else {
			Pl3xLibs.sendMessage(player, Lang.LORE_SET.get());
		}
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.lore");
	}
}
