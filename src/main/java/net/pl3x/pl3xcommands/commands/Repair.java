package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Player;

public class Repair implements MC_Command {
	public Repair(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.REPAIR.get();
	}

	@Override
	public String getCommandName() {
		return "repair";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7repair &a- &d" + Lang.REPAIR_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		MC_ItemStack hand = player.getItemInHand();
		if (hand == null || hand.getId() == 0) {
			Pl3xLibs.sendMessage(player, Lang.REPAIR_NO_ITEM.get());
			return;
		}
		if (hand.getDamage() == 0) {
			Pl3xLibs.sendMessage(player, Lang.REPAIR_ITEM_NO_DAMAGE.get());
			return;
		}
		hand.setDamage(0);
		player.setItemInHand(hand);
		Pl3xLibs.sendMessage(player, Lang.REPAIR_ITEM_REPAIRED.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.repair");
	}
}
