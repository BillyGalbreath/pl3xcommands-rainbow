package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class SetHome implements MC_Command {
	public SetHome(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.SETHOME.get();
	}

	@Override
	public String getCommandName() {
		return "sethome";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/sethome ((player:)home) - " + Lang.SETHOME_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.sethome.other")) {
			return Pl3xLibs.colorize("&e/&7sethome &e((&7player&b:&e)&7home&e) &a- &d" + Lang.SETHOME_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7sethome &e(&7home&e) &a- &d" + Lang.SETHOME_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		String homeName = (args.length > 0 ? args[0].trim() : "home").toLowerCase();
		if (homeName.contains(":")) {
			if (!player.hasPermission("command.sethome.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			String[] ss = homeName.split(":");
			if (ss.length < 2) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
				Pl3xLibs.sendMessage(player, getHelpLine(player));
				return;
			}
			MC_Player target = Pl3xLibs.getOfflinePlayer(ss[0]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.sethome.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.SETHOME_TARGET_EXEMPT.get());
				return;
			}
			homeName = ss[1];
			int count = Home.getHomesCount(target.getUUID());
			int limit = Pl3xLibs.getLimit(target, "command.sethome.limit");
			if (limit == 0) {
				limit = 1;
			}
			if (limit > 0 && count >= limit) {
				Pl3xLibs.sendMessage(player, Lang.SETHOME_LIMIT_REACHED_OTHER.get().replace("{target}", target.getCustomName()));
				return;
			}
			Home.setHomeLocation(target.getUUID(), homeName, player.getLocation());
		} else {
			int count = Home.getHomesCount(player.getUUID());
			int limit = Pl3xLibs.getLimit(player, "command.sethome.limit");
			if (limit == 0) {
				limit = 1;
			}
			if (limit > 0 && count >= limit) {
				Pl3xLibs.sendMessage(player, Lang.SETHOME_LIMIT_REACHED.get());
				return;
			}
			Home.setHomeLocation(player.getUUID(), homeName, player.getLocation());
		}
		Pl3xLibs.sendMessage(player, Lang.SETHOME_HOME_SET.get().replace("{home}", homeName));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.sethome");
	}
}
