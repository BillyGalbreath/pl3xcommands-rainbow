package net.pl3x.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Player;

public class ClearInventory implements MC_Command {
	public ClearInventory(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.CLEARINVENTORY.get();
	}

	@Override
	public String getCommandName() {
		return "clearinventory";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/clearinventory (player) - " + Lang.CLEARINVENTORY_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.clearinventory.other")) {
			return Pl3xLibs.colorize("&e/&7clearinventory &e(&7player&e) &a- &d" + Lang.CLEARINVENTORY_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7clearinventory &a- &d" + Lang.CLEARINVENTORY_HELP_DESC_SELF.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			if (player == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			player.setInventory(new ArrayList<MC_ItemStack>());
			Pl3xLibs.sendMessage(player, Lang.CLEARINVENTORY_CLEARED_SELF.get());
			return;
		}
		if (player != null && !player.hasPermission("command.clearinventory.other")) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
			return;
		}
		MC_Player target = Pl3xLibs.getPlayer(args[0]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		if (target.hasPermission("command.clearinventory.exempt")) {
			Pl3xLibs.sendMessage(player, Lang.CLEARINVENTORY_TARGET_EXEMPT.get());
			return;
		}
		target.setInventory(new ArrayList<MC_ItemStack>());
		Pl3xLibs.sendMessage(target, Lang.CLEARINVENTORY_CLEARED_BY_OTHER.get().replace("{player}", player == null ? "console" : player.getCustomName()));
		Pl3xLibs.sendMessage(player, Lang.CLEARINVENTORY_CLEARED_OTHER.get().replace("{player}", target.getCustomName()));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.clearinventory");
	}
}
