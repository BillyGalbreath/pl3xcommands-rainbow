package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Player;

public class Rename implements MC_Command {
	public Rename(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.RENAME.get();
	}

	@Override
	public String getCommandName() {
		return "rename";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7rename &e<&7name&e> &a- &d" + Lang.RENAME_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.RENAME_NO_NAME.get());
			return;
		}
		String newName = Pl3xLibs.colorize(Pl3xLibs.join(args, " ", 0));
		MC_ItemStack hand = player.getItemInHand();
		if (hand == null || hand.getId() == 0) {
			Pl3xLibs.sendMessage(player, Lang.RENAME_NO_ITEM.get());
			return;
		}
		// Limit name of "containers" to 32 length to prevent crashes
		switch (hand.getId()) {
			case 117: // brewing stand
			case 379: // brewing stand item
			case 23: // dispenser
			case 158: // dropper
			case 61: // furnace
			case 62: // burning furnace
			case 343: // minecart with furnace
			case 154: // hopper
			case 408: // minecart with hopper
			case 54: // chest
			case 342: // minecart with chest
			case 97: // monster egg
				if (newName.length() > 32) {
					newName = newName.substring(0, 32);
				}
				Pl3xLibs.sendMessage(player, Lang.RENAME_NAME_TOO_LONG.get().replace("{name}", newName));
		}
		hand.setCustomName(newName);
		player.setItemInHand(hand);
		Pl3xLibs.sendMessage(player, Lang.RENAME_RENAMED.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.rename");
	}
}
