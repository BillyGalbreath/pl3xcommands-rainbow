package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.TextFile;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Rules implements MC_Command {
	private MyPlugin plugin;

	public Rules(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.RULES.get();
	}

	@Override
	public String getCommandName() {
		return "rules";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/rules - " + Lang.RULES_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7rules &a- &d" + Lang.RULES_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		List<String> rules = TextFile.getLineByLine(plugin, "rules.txt");
		if (rules == null || rules.isEmpty()) {
			Pl3xLibs.sendMessage(player, Lang.RULES_NO_RULES.get());
		}
		for (String rule : rules) {
			Pl3xLibs.sendMessage(player, rule);
		}
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.rules");
	}
}
