package net.pl3x.pl3xcommands.commands;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Entity;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_World;

public class Butcher implements MC_Command {
	private MyPlugin plugin;

	public Butcher(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.BUTCHER.get();
	}

	@Override
	public String getCommandName() {
		return "butcher";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7butcher &e(&7&e) &e(&7radius&e) &a- &d" + Lang.BUTCHER_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		Set<Character> flags = new HashSet<Character>();
		int radius = 0;
		if (args.length > 0) {
			flags = getFlags(args);
			radius = getRadius(args);
		}
		int counter = 0;
		MC_Location pLoc = player.getLocation();
		MC_World world = player.getWorld();
		for (MC_Entity entity : world.getEntities()) {
			if (radius > 0 && entity.getLocation().distanceTo(pLoc) > radius) {
				continue;
			}
			if (isMob(entity)) {
				removeEntity(entity, flags.contains('l'));
				counter++;
				continue;
			}
			if (flags.contains('f') && (isPet(entity) || isNPC(entity) || isGolem(entity) || isAnimal(entity))) {
				removeEntity(entity, flags.contains('l'));
				counter++;
				continue;
			}
			if (flags.contains('p') && isPet(entity)) {
				removeEntity(entity, flags.contains('l'));
				counter++;
				continue;
			}
			if (flags.contains('a') && isAnimal(entity)) {
				removeEntity(entity, flags.contains('l'));
				counter++;
				continue;
			}
			if (flags.contains('n') && isNPC(entity)) {
				removeEntity(entity, flags.contains('l'));
				counter++;
				continue;
			}
			if (flags.contains('g') && isGolem(entity)) {
				removeEntity(entity, flags.contains('l'));
				counter++;
				continue;
			}
		}
		Pl3xLibs.sendMessage(player, Lang.BUTCHER_KILLED.get().replace("{count}", Integer.toString(counter)));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.butcher");
	}

	private boolean isMob(MC_Entity entity) {
		switch (entity.getType()) {
			case BLAZE:
			case CAVE_SPIDER:
			case CREEPER:
			case ENDERMAN:
			case ENDERMITE:
			case GHAST:
			case GIANT:
			case GUARDIAN:
			case LAVA_SLIME:
			case PIG_ZOMBIE:
			case SILVERFISH:
			case SKELETON:
			case SLIME:
			case SPIDER:
			case WITCH:
			case ZOMBIE:
				return true;
			default:
				return false;
		}
	}

	private boolean isPet(MC_Entity entity) {
		switch (entity.getType()) {
			case HORSE:
			case OCELOT:
			case WOLF:
				return true;
			default:
				return false;
		}
	}

	private boolean isAnimal(MC_Entity entity) {
		switch (entity.getType()) {
			case BAT:
			case CHICKEN:
			case COW:
			case HORSE:
			case MUSHROOM_COW:
			case OCELOT:
			case PIG:
			case RABBIT:
			case SHEEP:
			case SNOWMAN:
			case SQUID:
			case WOLF:
				return true;
			default:
				return false;
		}
	}

	private boolean isNPC(MC_Entity entity) {
		switch (entity.getType()) {
			case VILLAGER:
				return true;
			default:
				return false;
		}
	}

	private boolean isGolem(MC_Entity entity) {
		switch (entity.getType()) {
			case VILLAGER_GOLEM:
				return true;
			default:
				return false;
		}
	}

	private Set<Character> getFlags(String[] args) {
		Set<Character> list = new HashSet<Character>();
		for (String arg : args) {
			if (arg.startsWith("-")) {
				for (int i = 1; i < arg.length(); i++) {
					list.add(arg.charAt(i));
				}
			}
		}
		return list;
	}

	private int getRadius(String[] args) {
		int radius = 0;
		if (args.length > 0) {
			try {
				radius = Integer.valueOf(args[0]);
			} catch (NumberFormatException e1) {
				if (args.length > 1) {
					try {
						radius = Integer.valueOf(args[0]);
					} catch (NumberFormatException e2) {
						radius = -1;
					}
				}
			}
		}
		return radius;
	}

	private void removeEntity(MC_Entity entity, boolean lightning) {
		if (lightning) {
			MC_Location loc = entity.getLocation();
			plugin.getServer().executeCommand("/summon LightningBolt " + loc.x + " " + loc.y + " " + loc.z);
		}
		entity.removeEntity();
	}
}
