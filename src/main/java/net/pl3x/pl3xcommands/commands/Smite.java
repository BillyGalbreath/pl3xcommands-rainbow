package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Smite implements MC_Command {
	private MyPlugin plugin;

	public Smite(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.SMITE.get();
	}

	@Override
	public String getCommandName() {
		return "smite";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/smite <player | [<x> <y> <z>]> - " + Lang.SMITE_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.smite.other")) {
			return Pl3xLibs.colorize("&e/&7smite &e<&7player &e| [&7x y z&e]> &a- &d" + Lang.SMITE_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7smite &e<&7x&e> <&7y&e> <&7z&e> &a- &d" + Lang.SMITE_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 1) {
			// TODO smite where player is looking!
			// XXX
			//
			Pl3xLibs.sendMessage(player, Lang.SMITE_NO_ARGS.get());
			return;
		}
		if (args.length == 1) {
			// player
			if (player != null && !player.hasPermission("command.smite.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			MC_Player target = Pl3xLibs.getPlayer(args[0]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.smite.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.SMITE_TARGET_EXEMPT.get());
				return;
			}
			int x = target.getLocation().getBlockX();
			int y = target.getLocation().getBlockY();
			int z = target.getLocation().getBlockZ();
			plugin.getServer().executeCommand("/summon LightningBolt " + x + " " + y + " " + z);
			Pl3xLibs.sendMessage(target, Lang.SMITE_TARGET_MSG.get().replace("{player}", player == null ? "console" : player.getCustomName()));
			Pl3xLibs.sendMessage(player, Lang.SMITE_OTHER_MSG.get().replace("{target}", target.getCustomName()));
			return;
		}
		if (args.length < 3) {
			Pl3xLibs.sendMessage(player, Lang.SMITE_NOT_ENOUGH_ARGS.get());
			return;
		}
		// coords
		double x, y, z;
		try {
			x = Double.valueOf(args[0]);
			y = Double.valueOf(args[1]);
			z = Double.valueOf(args[2]);
		} catch (NumberFormatException e) {
			Pl3xLibs.sendMessage(player, Lang.SMITE_NOT_A_NUMBER.get());
			return;
		}
		plugin.getServer().executeCommand("/summon LightningBolt " + x + " " + y + " " + z);
		Pl3xLibs.sendMessage(player, Lang.SMITE_COORDS_MSG.get().replace("{x}", Double.toString(x)).replace("{y}", Double.toString(y)).replace("{z}", Double.toString(z)));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.smite");
	}
}
