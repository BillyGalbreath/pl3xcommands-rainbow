package net.pl3x.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_World;

public class World implements MC_Command {
	public static HashMap<UUID, MC_Location> respawnLocs = new HashMap<UUID, MC_Location>();

	public World(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.WORLD.get();
	}

	@Override
	public String getCommandName() {
		return "world";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7world (&e(&7world&e) &a- &d" + Lang.WORLD_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			List<String> list = new ArrayList<String>();
			for (String name : Pl3xLibs.getWorldNames()) {
				if (name.toLowerCase().startsWith(args[0].toLowerCase())) {
					list.add(name);
				}
			}
			return list;
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.WORLD_NOT_SPECIFIED.get());
			Pl3xLibs.sendMessage(player, Pl3xLibs.join(Pl3xLibs.getWorldNames(), ", ", 0));
			return;
		}
		String name = args[0].trim();
		MC_World world = Pl3xLibs.getWorld(name);
		if (world == null) {
			Pl3xLibs.sendMessage(player, Lang.WORLD_NO_WORLD_FOUND.get().replace("{name}", name));
			return;
		}
		player.teleport(Pl3xLibs.getSafeLocation(Pl3xLibs.getWorldSpawn(world)));
		Pl3xLibs.sendMessage(player, Lang.WORLD_TELEPORTED.get().replace("{name}", Pl3xLibs.getWorldName(world)));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.world");
	}
}
