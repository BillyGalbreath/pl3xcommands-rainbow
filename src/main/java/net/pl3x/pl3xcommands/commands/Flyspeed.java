package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Flyspeed implements MC_Command {
	public Flyspeed(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.FLYSPEED.get();
	}

	@Override
	public String getCommandName() {
		return "flyspeed";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/flyspeed <speed> (player) - " + Lang.FLYSPEED_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.flyspeed.other")) {
			return Pl3xLibs.colorize("&e/&7flyspeed &e<&7speed&e> (&7player&e) &a- &d" + Lang.FLYSPEED_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7flyspeed &e<&7speed&e> &a- &d" + Lang.FLYSPEED_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 2) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[1]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.FLYSPEED_NO_NUMBER.get());
			return;
		}
		float speed;
		try {
			speed = Float.valueOf(args[0]);
		} catch (NumberFormatException e) {
			Pl3xLibs.sendMessage(player, Lang.FLYSPEED_INVALID_NUMBER.get());
			return;
		}
		if (speed < 0 || speed > 10) {
			Pl3xLibs.sendMessage(player, Lang.FLYSPEED_BAD_RANGE_NUMBER.get());
			return;
		}
		if (args.length > 1) {
			if (player != null && !player.hasPermission("command.flyspeed.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			MC_Player target = Pl3xLibs.getPlayer(args[1]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.flyspeed.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.FLYSPEED_TARGET_EXEMPT.get());
				return;
			}
			target.setFlySpeed(speed / 10F);
			Pl3xLibs.sendMessage(target, Lang.FLYSPEED_SPEED_SET.get());
			Pl3xLibs.sendMessage(player, Lang.FLYSPEED_SPEED_SET_OTHER.get().replace("{target}", target.getCustomName()));
			return;
		}
		player.setFlySpeed(speed / 10F);
		Pl3xLibs.sendMessage(player, Lang.FLYSPEED_SPEED_SET.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.flyspeed");
	}
}
