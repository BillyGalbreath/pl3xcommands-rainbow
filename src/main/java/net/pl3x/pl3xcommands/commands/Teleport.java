package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Teleport implements MC_Command {
	private MyPlugin plugin;

	public Teleport(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.TELEPORT.get();
	}

	@Override
	public String getCommandName() {
		return "teleport";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7teleport &e<&7player&e> &a- &d" + Lang.TP_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.TP_NO_PLAYER.get());
			return;
		}
		MC_Player target = plugin.getServer().getOnlinePlayerByName(args[0]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		if (PlayerConfig.getConfig(plugin, target.getUUID()).getBoolean("tp-disabled") && !player.hasPermission("command.tp.override")) {
			Pl3xLibs.sendMessage(player, Lang.TP_PLAYER_DISABLED_TP.get());
		}
		player.teleport(target.getLocation());
		Pl3xLibs.sendMessage(player, Lang.TP_TELEPORTED.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.teleport");
	}
}
