package net.pl3x.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Location;
import PluginReference.MC_Player;

public class Home implements MC_Command {
	public Home(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.HOME.get();
	}

	@Override
	public String getCommandName() {
		return "home";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/home ((player:)home) - " + Lang.HOME_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.home.other")) {
			return Pl3xLibs.colorize("&e/&7home &e((&7player&b:&e)&7home&e) &a- &d" + Lang.HOME_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7home &e(&7home&e) &a- &d" + Lang.HOME_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return getMatchingHomeNames(player.getUUID(), args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		String homeName = (args.length > 0 ? args[0].trim() : "home").toLowerCase();
		MC_Location homeLoc = null;
		if (homeName.contains(":")) {
			if (!player.hasPermission("command.home.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			String[] ss = homeName.split(":");
			if (ss.length < 2) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
				Pl3xLibs.sendMessage(player, getHelpLine(player));
				return;
			}
			MC_Player target = Pl3xLibs.getOfflinePlayer(ss[0]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.home.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.HOME_TARGET_EXEMPT.get());
				return;
			}
			homeName = ss[1];
			homeLoc = getHomeLocation(target.getUUID(), homeName);
		} else {
			if (player == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
				Pl3xLibs.sendMessage(player, getHelpLine(player));
				return;
			}
			homeLoc = getHomeLocation(player.getUUID(), homeName);
		}
		if (homeLoc == null) {
			Pl3xLibs.sendMessage(player, Lang.HOME_NOT_SET.get());
			return;
		}
		player.teleport(homeLoc);
		Pl3xLibs.sendMessage(player, Lang.HOME_TELEPORTED.get().replace("{home}", homeName));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.home");
	}

	public static Integer getHomesCount(UUID uuid) {
		HashMap<String, String> mapHomes = PlayerConfig.getConfig(MyPlugin.getInstance(), uuid).getMap("homes");
		return mapHomes.size();
	}

	public static MC_Location getHomeLocation(UUID uuid, String home) {
		HashMap<String, String> mapHomes = PlayerConfig.getConfig(MyPlugin.getInstance(), uuid).getMap("homes");
		if (!mapHomes.containsKey(home)) {
			return null;
		}
		return Pl3xLibs.stringToLocation(mapHomes.get(home));
	}

	public static List<String> getMatchingHomeNames(UUID uuid, String home) {
		List<String> list = new ArrayList<String>();
		HashMap<String, String> mapHomes = PlayerConfig.getConfig(MyPlugin.getInstance(), uuid).getMap("homes");
		for (String key : mapHomes.keySet()) {
			if (key.toLowerCase().startsWith(home.toLowerCase())) {
				list.add(key);
			}
		}
		return list;
	}

	public static void setHomeLocation(UUID uuid, String home, MC_Location location) {
		PlayerConfig config = PlayerConfig.getConfig(MyPlugin.getInstance(), uuid);
		HashMap<String, String> mapHomes = config.getMap("homes");
		mapHomes.put(home, Pl3xLibs.locationToString(location));
		config.setMap("homes", mapHomes);
	}

	public static boolean deleteHomeLocation(UUID uuid, String home) {
		PlayerConfig config = PlayerConfig.getConfig(MyPlugin.getInstance(), uuid);
		HashMap<String, String> mapHomes = PlayerConfig.getConfig(MyPlugin.getInstance(), uuid).getMap("homes");
		if (!mapHomes.containsKey(home)) {
			return false;
		}
		mapHomes.remove(home);
		config.setMap("homes", mapHomes);
		return true;
	}
}
