package net.pl3x.pl3xcommands.runnables;

import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Player;

public class OnJoinThunder extends Pl3xRunnable {
	private MyPlugin plugin;
	private MC_Player player;

	public OnJoinThunder(MyPlugin plugin, MC_Player player) {
		this.plugin = plugin;
		this.player = player;
	}

	@Override
	public void run() {
		if (player == null) {
			return;
		}
		if (player.hasPermission("onjoin.kachigga")) {
			for (MC_Player target : plugin.getServer().getPlayers()) {
				target.playSound("ambient.weather.thunder", 1F, 1F);
			}
		}
	}
}
