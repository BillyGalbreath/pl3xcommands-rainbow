package net.pl3x.pl3xcommands.runnables;

import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import Pl3xCommands.MyPlugin;

public class AutoSaveData extends Pl3xRunnable {
	private MyPlugin plugin;

	public AutoSaveData(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		if (plugin.getConfig().getBoolean("debug-mode", false)) {
			plugin.getLogger().debug("Auto saving Pl3xCommands data!");
		}
		plugin.getWarpConfig().save();
		PlayerConfig.saveAllConfigs();
	}
}
