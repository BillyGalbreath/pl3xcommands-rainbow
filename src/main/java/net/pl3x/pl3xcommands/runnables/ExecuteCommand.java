package net.pl3x.pl3xcommands.runnables;

import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import PluginReference.MC_Player;

public class ExecuteCommand extends Pl3xRunnable {
	private MC_Player player;
	private String command;

	public ExecuteCommand(MC_Player player, String command) {
		this.player = player;
		this.command = command;
	}

	@Override
	public void run() {
		if (player == null) {
			return;
		}
		if (command == null || command.equals("")) {
			return;
		}
		player.executeCommand(command);
	}
}
