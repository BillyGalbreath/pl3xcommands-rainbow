package Pl3xCommands;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.MailBox;
import net.pl3x.pl3xcommands.TextFile;
import net.pl3x.pl3xcommands.commands.Back;
import net.pl3x.pl3xcommands.commands.Butcher;
import net.pl3x.pl3xcommands.commands.ClearInventory;
import net.pl3x.pl3xcommands.commands.Coords;
import net.pl3x.pl3xcommands.commands.DelHome;
import net.pl3x.pl3xcommands.commands.DelWarp;
import net.pl3x.pl3xcommands.commands.Fly;
import net.pl3x.pl3xcommands.commands.Flyspeed;
import net.pl3x.pl3xcommands.commands.God;
import net.pl3x.pl3xcommands.commands.Hat;
import net.pl3x.pl3xcommands.commands.Home;
import net.pl3x.pl3xcommands.commands.ListHomes;
import net.pl3x.pl3xcommands.commands.Lore;
import net.pl3x.pl3xcommands.commands.MOTD;
import net.pl3x.pl3xcommands.commands.Mail;
import net.pl3x.pl3xcommands.commands.Near;
import net.pl3x.pl3xcommands.commands.Nick;
import net.pl3x.pl3xcommands.commands.Ping;
import net.pl3x.pl3xcommands.commands.Pl3xCommands;
import net.pl3x.pl3xcommands.commands.PowerTool;
import net.pl3x.pl3xcommands.commands.Rename;
import net.pl3x.pl3xcommands.commands.Repair;
import net.pl3x.pl3xcommands.commands.Rules;
import net.pl3x.pl3xcommands.commands.SetHome;
import net.pl3x.pl3xcommands.commands.SetSpawn;
import net.pl3x.pl3xcommands.commands.SetWarp;
import net.pl3x.pl3xcommands.commands.SetWorldSpawn;
import net.pl3x.pl3xcommands.commands.Smite;
import net.pl3x.pl3xcommands.commands.Spawn;
import net.pl3x.pl3xcommands.commands.Sudo;
import net.pl3x.pl3xcommands.commands.Suicide;
import net.pl3x.pl3xcommands.commands.Teleport;
import net.pl3x.pl3xcommands.commands.Top;
import net.pl3x.pl3xcommands.commands.Tp2p;
import net.pl3x.pl3xcommands.commands.Tpa;
import net.pl3x.pl3xcommands.commands.Tpaall;
import net.pl3x.pl3xcommands.commands.Tpaccept;
import net.pl3x.pl3xcommands.commands.Tpahere;
import net.pl3x.pl3xcommands.commands.Tpall;
import net.pl3x.pl3xcommands.commands.Tpdeny;
import net.pl3x.pl3xcommands.commands.Tphere;
import net.pl3x.pl3xcommands.commands.Tppos;
import net.pl3x.pl3xcommands.commands.Tptoggle;
import net.pl3x.pl3xcommands.commands.Walkspeed;
import net.pl3x.pl3xcommands.commands.Warp;
import net.pl3x.pl3xcommands.commands.World;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xcommands.configuration.WarpConfig;
import net.pl3x.pl3xcommands.runnables.AutoSaveData;
import net.pl3x.pl3xcommands.runnables.ExecuteCommand;
import net.pl3x.pl3xcommands.runnables.MailChecker;
import net.pl3x.pl3xcommands.runnables.OnJoinThunder;
import net.pl3x.pl3xcommands.runnables.StartMetrics;
import net.pl3x.pl3xcommands.runnables.TeleportPlayer;
import net.pl3x.pl3xlibs.Logger;
import net.pl3x.pl3xlibs.Material;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import PluginReference.MC_Command;
import PluginReference.MC_DamageType;
import PluginReference.MC_DirectionNESWUD;
import PluginReference.MC_EventInfo;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private static MyPlugin instance;
	private Logger logger = null;
	private BaseConfig config = null;
	private WarpConfig warpConfig = null;
	private MC_Server server;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Pl3xCommands";
		info.version = "0.1-BETA13-SNAPSHOT";
		info.description = "A collection of useful commands for any server";
		info.optionalData.put("plugin-directory", "plugins_mod" + File.separator + info.name + File.separator);
		return info;
	}

	public static MyPlugin getInstance() {
		return instance;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		instance = this;
		init();
		registerCommands();
		getLogger().info(getPluginInfo().name + " v" + getPluginInfo().version + " by BillyGalbreath is now enabled.");
	}

	@Override
	public void onShutdown() {
		disable();
		getLogger().info("Plugin disabled.");
	}

	public Logger getLogger() {
		if (logger == null) {
			logger = Pl3xLibs.getLogger(getPluginInfo());
		}
		return logger;
	}

	public MC_Server getServer() {
		return server;
	}

	public BaseConfig getConfig() {
		if (config == null) {
			config = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "config.ini");
			config.load();
		}
		return config;
	}

	public WarpConfig getWarpConfig() {
		if (warpConfig == null) {
			warpConfig = new WarpConfig(this);
		}
		return warpConfig;
	}

	private void init() {
		getConfig(); // initialize the config.ini file

		TextFile.copyFileFromJarToDisk(this, "lang-en.ini", true); // always overwrite default language file!
		TextFile.copyFileFromJarToDisk(this, "aliases.ini");
		TextFile.copyFileFromJarToDisk(this, "rules.txt");
		TextFile.copyFileFromJarToDisk(this, "motd.txt");

		Pl3xLibs.getScheduler().scheduleRepeatingTask(getPluginInfo().name, new MailChecker(this), 20, getConfig().getInteger("mail-check-delay", 300) * 20);
		Pl3xLibs.getScheduler().scheduleRepeatingTask(getPluginInfo().name, new AutoSaveData(this), 20, getConfig().getInteger("auto-save-delay", 15) * 1200);
		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new StartMetrics(this), 100);
	}

	private void disable() {
		Pl3xLibs.getScheduler().cancelAllRunnables(getPluginInfo().name);
		warpConfig.save();
		PlayerConfig.saveAllConfigs();
		PlayerConfig.removeAllConfigs();
		config = null;
		warpConfig = null;
		logger = null;
	}

	public void reload() {
		disable();
		init();
		Alias.refreshAll();
		Lang.refreshAll();
	}

	private void registerCommands() {
		registerCommand(new Back(this));
		registerCommand(new Butcher(this));
		registerCommand(new ClearInventory(this));
		registerCommand(new Coords(this));
		registerCommand(new DelHome(this));
		registerCommand(new DelWarp(this));
		registerCommand(new Fly(this));
		registerCommand(new Flyspeed(this));
		registerCommand(new God(this));
		registerCommand(new Hat(this));
		registerCommand(new Home(this));
		registerCommand(new ListHomes(this));
		registerCommand(new Lore(this));
		registerCommand(new Mail(this));
		registerCommand(new MOTD(this));
		registerCommand(new Near(this));
		registerCommand(new Nick(this));
		registerCommand(new Ping(this));
		registerCommand(new Pl3xCommands(this));
		registerCommand(new PowerTool(this));
		registerCommand(new Rename(this));
		registerCommand(new Repair(this));
		registerCommand(new Rules(this));
		registerCommand(new SetHome(this));
		registerCommand(new SetSpawn(this));
		registerCommand(new SetWarp(this));
		registerCommand(new SetWorldSpawn(this));
		registerCommand(new Smite(this));
		registerCommand(new Spawn(this));
		registerCommand(new Sudo(this));
		registerCommand(new Suicide(this));
		registerCommand(new Top(this));
		registerCommand(new Teleport(this));
		registerCommand(new Tp2p(this));
		registerCommand(new Tpa(this));
		registerCommand(new Tpaall(this));
		registerCommand(new Tpaccept(this));
		registerCommand(new Tpahere(this));
		registerCommand(new Tpall(this));
		registerCommand(new Tpdeny(this));
		registerCommand(new Tphere(this));
		registerCommand(new Tppos(this));
		registerCommand(new Tptoggle(this));
		registerCommand(new Walkspeed(this));
		registerCommand(new Warp(this));
		registerCommand(new World(this));
	}

	private void registerCommand(MC_Command command) {
		String commandName = command.getCommandName();
		List<String> list = getConfig().getStringList("disabled-commands");
		if (list != null && list.contains(commandName)) {
			if (getConfig().getBoolean("debug-mode", false)) {
				getLogger().debug("Disabled command NOT registered: " + commandName);
			}
			return;
		}
		getServer().registerCommand(command);
		if (getConfig().getBoolean("debug-mode", false)) {
			getLogger().debug("Registered command: " + commandName);
		}
	}

	@Override
	public void onPlayerJoin(MC_Player player) {
		UUID uuid = player.getUUID();
		PlayerConfig config = PlayerConfig.getConfig(this, uuid);
		MC_Location lastLocation = config.getLocation("last-location");
		if (lastLocation == null) {
			if (getConfig().getBoolean("debug-mode", false)) {
				getLogger().debug("New user: Sending to spawn!");
			}
			Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new TeleportPlayer(this, player, Pl3xLibs.getServerSpawn()), 1);
			if (getConfig().getBoolean("announce-new-users", true)) {
				for (MC_Player online : getServer().getPlayers()) {
					if (Pl3xLibs.decolorize(online.getCustomName()).equalsIgnoreCase(Pl3xLibs.decolorize(player.getCustomName()))) {
						continue; // do not send welcome message to the new user
					}
					Pl3xLibs.sendMessage(online, Lang.WELCOME_NEW_USER.get().replace("{player}", player.getCustomName()));
				}
			}
			String runCommand = getConfig().get("new-player-execute-command", null);
			if (runCommand != null && runCommand != "") {
				Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new ExecuteCommand(player, runCommand), 20);
			}
		} else {
			if (getConfig().getBoolean("debug-mode", false)) {
				getLogger().debug("Repeat user: Sending to last known location!");
			}
			Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new TeleportPlayer(this, player, lastLocation), 1);
			Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new OnJoinThunder(this, player), 20);
		}
		if (player.hasPermission("command.motd.onjoin")) {
			List<String> motd = TextFile.getLineByLine(this, "motd.txt");
			if (motd != null) {
				for (String line : MOTD.replaceVars(this, motd, player)) {
					Pl3xLibs.sendMessage(player, line);
				}
			}
		}
		String nickname = config.get("nickname");
		if (nickname != null && !nickname.equals("")) {
			player.setCustomName(nickname);
		}
		if (player.hasPermission("command.rules.onjoin")) {
			List<String> rules = TextFile.getLineByLine(this, "rules.txt");
			if (rules != null) {
				for (String rule : rules) {
					Pl3xLibs.sendMessage(player, rule);
				}
			}
		}
		MailBox mailbox = MailBox.getMailBox(this, uuid);
		if (!mailbox.refreshMail().isEmpty()) {
			Pl3xLibs.sendMessage(player, Lang.MAIL_NOTIFICATION_ALERT.get());
		}
	}

	@Override
	public void onPlayerLogout(String playerName, UUID uuid) {
		// remove player from TPA db
		for (Iterator<Map.Entry<UUID, UUID>> iter = Tpa.tpadb.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<UUID, UUID> entry = iter.next();
			if (entry.getKey().equals(uuid) || entry.getValue().equals(uuid)) {
				iter.remove();
			}
		}
		// remove player from TPAHERE db
		for (Iterator<Map.Entry<UUID, UUID>> iter = Tpahere.tpaheredb.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<UUID, UUID> entry = iter.next();
			if (entry.getKey().equals(uuid) || entry.getValue().equals(uuid)) {
				iter.remove();
			}
		}
		// remove player from BACK db
		if (Back.backdb.containsKey(playerName)) {
			Back.backdb.remove(playerName);
		}
		// Save last known location
		MC_Player player = Pl3xLibs.getPlayer(uuid);
		if (player != null) {
			PlayerConfig.getConfig(this, uuid).set("last-location", Pl3xLibs.locationToString(player.getLocation()));
		}
	}

	@Override
	public void onPlayerDeath(MC_Player plrVictim, MC_Player plrKiller, MC_DamageType dmgType, String deathMsg) {
		Back.backdb.put(plrVictim.getCustomName(), plrVictim.getLocation());
		MC_Location bedLocation = plrVictim.getBedRespawnLocation();
		if (bedLocation != null) {
			if (Pl3xLibs.getBlock(bedLocation).getId() == Material.BED_BLOCK.getId()) {
				World.respawnLocs.put(plrVictim.getUUID(), bedLocation);
				return;
			}
		}
		if (getConfig().getBoolean("teleport-to-world-spawn-on-death", false)) {
			if (getConfig().getBoolean("debug-mode", false)) {
				getLogger().debug("Player died. Will respawn in same world");
			}
			World.respawnLocs.put(plrVictim.getUUID(), Pl3xLibs.getWorldSpawn(plrVictim.getWorld()));
			return;
		}
		if (getConfig().getBoolean("debug-mode", false)) {
			getLogger().debug("Player died. Will respawn at server spawn!");
		}
		World.respawnLocs.put(plrVictim.getUUID(), Pl3xLibs.getServerSpawn());
	}

	@Override
	public void onPlayerRespawn(MC_Player player) {
		MC_Location respawn = World.respawnLocs.get(player.getUUID());
		if (respawn == null) {
			respawn = Pl3xLibs.getServerSpawn(); // this should never happen, but just in case
		}
		if (getConfig().getBoolean("debug-mode", false)) {
			getLogger().debug("Player respawned: " + Pl3xLibs.locationToString(player.getLocation()));
		}
		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new TeleportPlayer(this, player, respawn), 1);
	}

	@Override
	public void onAttemptPlayerTeleport(MC_Player player, MC_Location loc, MC_EventInfo ei) {
		if (ei.isCancelled) {
			return;
		}
		if (loc.dimension == -1 && (int) loc.y >= 128 && !player.hasPermission("netherroof.exempt")) {
			player.teleport(Pl3xLibs.getWorldSpawn(player.getWorld()));
			Pl3xLibs.sendMessage(player, Lang.NETHERROOF_NOT_ALLOWED.get());
			ei.isCancelled = true;
			return;
		}
		MC_Location previous = Back.backdb.get(player.getCustomName());
		if (previous != null && player.getLocation().equals(previous)) {
			Back.backdb.remove(player.getCustomName());
			return;
		}
		Back.backdb.put(player.getCustomName(), player.getLocation());
	}

	@Override
	public void onAttemptPlayerMove(MC_Player player, MC_Location from, MC_Location to, MC_EventInfo ei) {
		if (to.dimension == -1 && (int) to.y >= 128 && !player.hasPermission("netherroof.exempt")) {
			player.teleport(Pl3xLibs.getWorldSpawn(player.getWorld()));
			Pl3xLibs.sendMessage(player, Lang.NETHERROOF_NOT_ALLOWED.get());
			ei.isCancelled = true;
		}
	}

	@Override
	public void onAttemptPlaceOrInteract(MC_Player player, MC_Location loc, MC_EventInfo ei, MC_DirectionNESWUD dir) {
		if (ei.isCancelled) {
			return;
		}
		MC_ItemStack hand = player.getItemInHand();
		if (hand == null || hand.getId() == 0) {
			return;
		}
		PlayerConfig config = PlayerConfig.getConfig(this, player.getUUID());
		HashMap<String, String> tools = config.getPowerTools();
		String currentTool = tools.get(hand.getFriendlyName());
		if (currentTool == null || currentTool.equals("")) {
			return;
		}
		player.executeCommand("/" + currentTool);
		ei.isCancelled = true;
	}
}
